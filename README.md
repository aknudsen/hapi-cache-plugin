# Hapi.js Caching Plugin
This is a caching plugin for the Hapi.js framework. It's a facade in front of storage backends,
which uses the concept of a policy to call a value generator if an entry is missing from the 
cache (and insert the value into the cache).