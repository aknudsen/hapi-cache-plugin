const {SError,} = require('error')

class ImplementationError extends SError {
  static create(message, info) {
    return super.create(message || 'Implementation error', info)
  }
}

class ArgumentError extends SError {
  static create(message, info) {
    return super.create(message || 'Argument error', info)
  }
}

module.exports = {
  ImplementationError,
  ArgumentError,
}
