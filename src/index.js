// A cache manager plugin for Hapi
const t = require('tcomb')
const pick = require('ramda/src/pick')
const omit = require('ramda/src/omit')
const concat = require('ramda/src/concat')
const keys = require('ramda/src/keys')
const Promise = require('bluebird')

const pkg = require('../package.json')
const {ArgumentError,} = require('./errors')

const DEFAULTS = {
  expiresIn: 60,
  generateTimeout: 5 * 1000,
}

const CACHE_SETTING_KEYS = ['expiresIn', 'generateTimeout',]

// Register Hapi.js plugin
const register = async (server, options) => {
  t.Object(server, ['server',])
  t.Function(server.decorate, ['server', 'decorate',])

  const settings = {
    ...DEFAULTS,
    ...(options || {}),
  }

  const logger = server.logger
  logger.debug({settings,}, `Registering cache manager plugin, engine: ${settings.engine}`)

  const engine = options.engine !== 'bypass' ? require('./aerospikeEngine') :
    require('./bypassEngine')
  const client = await engine(omit(concat(['engine',], CACHE_SETTING_KEYS), options))

  const cacheManager = {
    policies: new Map(),
    client,
    ...pick(CACHE_SETTING_KEYS, settings),
  }

  server.ext({
    type: 'onPreStart',
    method: async () => {
      logger.debug(`Starting cache`)
      await client.start(logger)
      await Promise.map(cacheManager.policies.values(), async (policy) => {
        await Promise.map(policy.indexOn || [], async (fieldName) => {
          await cacheManager.client.createIndex(policy.name, fieldName, logger)
        })
      })
    },
  })
  server.ext({
    type: 'onPostStop',
    method: async () => {
      logger.debug(`Stopping cache`)
      await client.stop(logger)
    },
  })

  server.decorate('server', 'createCachePolicy', (options) => {
    return createCachePolicy(cacheManager, options, server)
  })
}

const createCachePolicy = async (manager, options, server) => {
  t.Object(manager, ['manager',])
  t.Object(options, ['options',])
  t.String(options.name, ['options', 'name',])
  t.Function(options.generateFunc, ['options', 'generateFunc',])
  t.Object(server, ['server',])
  t.Object(server.logger, ['server', 'logger',])
  if (options.indexOn != null) {
    t.Array(options.indexOn, ['options', 'indexOn',])
  }

  const logger = server.logger

  if (manager.policies.has(options.name)) {
    throw ArgumentError.create(`Policy ${options.name} already exists`)
  }

  logger.debug(`Creating cache policy ${options.name}`)
  const policy = {
    name: options.name,
    pendings: new Map(),
    client: manager.client,
    ...pick(['generateFunc', 'indexOn',], options),
    ...pick(CACHE_SETTING_KEYS, manager),
  }
  policy.get = async (key, logger) => {
    return await getFromCache(policy, key, logger)
  }
  policy.drop = async (key, logger) => {
    await policy.client.drop(policy.name, key.id, logger)
  }
  policy.dropByIndex = async (fieldName, fieldValue, logger) => {
    await policy.client.dropByIndex(policy.name, fieldName, fieldValue, logger)
  }

  manager.policies.set(options.name, policy)

  return policy
}

const getId = (cachePolicy, key) => {
  t.Object(cachePolicy, ['cachePolicy',])
  t.Object(key, ['key',])
  return `${cachePolicy.name}/${key.id}`
}

const generateValue = async (cachePolicy, key, logger) => {
  t.Object(cachePolicy, ['cachePolicy',])
  t.Function(cachePolicy.generateFunc, ['cachePolicy', 'generateFunc',])
  t.Object(cachePolicy.client, ['cachePolicy', 'client',])
  t.Object(key, ['key',])
  t.String(key.id, ['key', 'id',])
  t.Object(logger, ['logger',])
  if (cachePolicy.indexOn != null) {
    t.Array(cachePolicy.indexOn, ['cachePolicy', 'indexOn',])
  }

  const value = await cachePolicy.generateFunc(key, logger)
  logger.debug({valueKeys: keys(value),}, `Value received from generateFunc`)

  const ttl = cachePolicy.expiresIn
  const id = getId(cachePolicy, key)
  logger.debug({id, ttl, itemKeys: keys(value),}, `Setting value in client`)
  await cachePolicy.client.set(cachePolicy.name, key.id, value, ttl, cachePolicy.indexOn, logger)
  return value
}

const getFromCache = async (cachePolicy, key, logger) => {
  t.Object(cachePolicy, ['cachePolicy',])
  t.Object(key, ['key',])
  t.String(key.id, ['key', 'id',])
  t.Object(logger, ['logger',])

  const id = getId(cachePolicy, key)
  logger.debug(`Trying to get entry ${id} from cache`)
  const pending = cachePolicy.pendings.get(id)
  if (pending != null) {
    logger.debug(`Awaiting pending promise for cache entry ${id}`)
    return await pending
  } else {
    logger.debug({cachePolicy: cachePolicy.name, id,},
      `Getting value for entry ${id} from client`)
    try {
      const record = await cachePolicy.client.get(cachePolicy.name, key.id, logger)
      if (record != null) {
        logger.debug(`Entry ${id} retrieved from cache`)
        t.Object(record.bins, ['record', 'bins',])
        t.Number(record.ttl, ['record', 'ttl',])
        if (record.ttl <= 0) {
          logger.debug(
            `Cache entry ${id} has expired, dropping it`)
          await cachePolicy.client.drop(cachePolicy.name, key.id, logger)
        } else {
          logger.debug(`Returning cache entry ${id} as it's not yet expired`)
          return JSON.parse(record.bins.item)
        }
      } else {
        logger.debug(`No cached entry ${cachePolicy.name}/${id}`)
      }
    } catch (err) {
      logger.warn({err,}, `Caught exception getting entry from cache`)
      logger.debug(`Dropping entry causing exception`)
      await cachePolicy.client.drop(cachePolicy.name, key.id, logger)
    }

    logger.debug(`Generating value for cache entry ${id} - adding to pending promises`)
    const promise = generateValue(cachePolicy, key, logger)
    cachePolicy.pendings.set(id, promise)
    try {
      return await promise
    } finally {
      logger.debug(`Deleting pending promise for cache entry ${id}`)
      cachePolicy.pendings.delete(id)
    }
  }
}

module.exports = {
  plugin: {
    name: 'cacheManager',
    version: pkg.version,
    register,
  },
}
