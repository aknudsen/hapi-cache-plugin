const t = require('tcomb')
const aerospike = require('aerospike')
const reduce = require('ramda/src/reduce')
const path = require('path')

const {ImplementationError,} = require('./errors')

const DEFAULTS = {
  hosts: [{
    addr: 'localhost',
    port: 3000,
  },],
  namespace: 'test',
}

module.exports = (options) => {
  const settings = {
    ...DEFAULTS,
    ...(options || {}),
  }
  const state = {
    settings,
    db: null,
  }

  const start = async (logger) => {
    if (state.db == null) {
      const connectOpts = {
        hosts: settings.hosts,
      }
      logger.debug({connectOpts,}, `Connecting to Aerospike`)
      state.db = await aerospike.connect(connectOpts)
      logger.debug(`Registering UDF`)
      const job = await state.db.udfRegister(path.join(__dirname, 'remove_index_entries.lua'))
      await job.wait()
    }
  }

  const stop = async () => {
    await state.db.close()
    state.db = null
  }

  const get = async (policy, id, logger) => {
    t.String(policy, ['policy',])
    t.String(id, ['id',])
    t.Object(logger, ['logger',])

    if (state.db == null) {
      throw ImplementationError.create('Connection not started')
    }

    logger.debug(`Getting entry ${policy}/${id}`)
    try {
      return await state.db.get(new aerospike.Key(settings.namespace, policy, id))
    } catch (err) {
      if (/Record does not exist in database/.test(err.message)) {
        logger.debug(`Couldn't find entry in database`)
        return null
      } else {
        throw err
      }
    }
  }

  const set = async (policyName, id, item, ttl, indexOn, logger) => {
    t.String(policyName, ['policyName',])
    t.String(id, ['id',])
    t.Any(item, ['item',])
    t.Number(ttl, ['ttl',])
    t.Object(logger, ['logger',])
    if (indexOn != null) {
      t.Array(indexOn, ['indexOn',])
    }

    if (state.db == null) {
      throw ImplementationError.create('Connection not started')
    }

    const itemJson = JSON.stringify(item)
    const indexFields = reduce((acc, fieldName) => {
      const key = `index:${fieldName}`
      return {
        ...acc,
        [key]: item[fieldName] || '',
      }
    }, {}, indexOn || [])
    const envelope = {
      item: itemJson,
      ...indexFields,
    }
    logger.debug(`Storing envelope in Aerospike at ${policyName}/${id}`)
    try {
      await state.db.put(new aerospike.Key(settings.namespace, policyName, id), envelope, {
        ttl,
      })
    } catch (err) {
      logger.error({err,}, `Failed to store envelope (size: ${itemJson.length}) in Aerospike`)
    }
  }

  const drop = async (policy, id, logger) => {
    t.String(policy, ['policy',])
    t.String(id, ['id',])
    t.Object(logger, ['logger',])

    if (state.db == null) {
      throw ImplementationError.create('Connection not started')
    }

    logger.debug(`Deleting cache entry ${policy}/${id}`)
    try {
      await state.db.remove(new aerospike.Key(settings.namespace, policy, id))
    } catch (err) {
      logger.debug({err,}, `Failed deleting cache entry ${policy}/${id}`)
    }
  }

  const createIndex = async (policyName, fieldName, logger) => {
    if (state.db == null) {
      throw ImplementationError.create('Connection not started')
    }

    const indexOpts = {
      ns: settings.namespace,
      set: policyName,
      bin: `index:${fieldName}`,
      index: fieldName,
    }
    logger.debug({indexOpts,}, `Creating Aerospike index...`)
    let job
    try {
      job = await state.db.createStringIndex(indexOpts)
    } catch (err) {
      if (/Index with the same name already exists/.test(err.message)) {
        logger.debug(`The index already exists`)
        return
      } else {
        logger.error({err,}, `An error occurred while creating index`)
        throw err
      }
    }

    await job.wait()
  }

  const dropByIndex = async (policyName, fieldName, fieldValue, logger) => {
    t.String(policyName, ['policyName',])
    t.String(fieldName, ['fieldName',])
    t.String(fieldValue, ['fieldValue',])
    t.Object(logger, ['logger',])
    t.String(fieldValue, ['fieldValue',])

    if (state.db == null) {
      throw ImplementationError.create('Connection not started')
    }

    logger.debug(
      `Dropping cache entries within policy ${policyName}, which field ${fieldName} equals ${
          fieldValue}`)
    const query = state.db.query(settings.namespace, policyName, {
      filters: [
        aerospike.filter.equal(`index:${fieldName}`, fieldValue),
      ],
    })
    await query.background('remove_index_entries', 'remove')
  }

  return {
    start,
    stop,
    get,
    set,
    drop,
    dropByIndex,
    createIndex,
  }
}
