module.exports = () => {
  const start = () => {}

  const stop = () => {}

  const get = () => {
    return null
  }

  const set = () => {}

  const drop = () => {}

  const createIndex = () => {}

  const dropByIndex = () => {}

  return {
    start,
    stop,
    get,
    set,
    drop,
    dropByIndex,
    createIndex,
  }
}
